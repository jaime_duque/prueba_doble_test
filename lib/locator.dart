import 'package:get_it/get_it.dart';
import 'package:stacked/stacked.dart';

GetIt locator = GetIt.instance;

Future<void> setupLocator() async {
  /// [singleton-locator]
  if (!locator.isRegistered<TestService>()) {
    locator.registerLazySingleton(() => TestService());
  }
}

class TestService with ReactiveServiceMixin {
  TestService() {
    listenToReactiveValues(<dynamic>[_email]);
  }

  final ReactiveValue<String?> _email = ReactiveValue<String?>(null);
  String? get email => _email.value;
}

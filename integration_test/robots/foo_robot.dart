import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';

class FooRobot {
  FooRobot(this.tester);

  final WidgetTester tester;

  Finder title = find.byKey(const Key('title'));
  Finder exampleText = find.byKey(const Key('text'));

  Future<void> checkTitle() async {
    await tester.pumpAndSettle();
    Text text = tester.widget(title);
    await tester.pumpAndSettle();
    expect(text.data, 'You have pushed the button this many times:');
  }

  Future<void> checkCounter() async {
    await tester.pumpAndSettle();
    Text text = tester.widget(exampleText);
    await tester.pumpAndSettle();
    expect(text.data, 'Test text');
  }
}

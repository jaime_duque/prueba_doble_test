import 'package:flutter_test/flutter_test.dart';
import 'package:integration_test/integration_test.dart';
import 'package:prueba_doble_test/main.dart' as app;

import 'robots/foo_robot.dart';

void main() {
  final config = IntegrationTestWidgetsFlutterBinding.ensureInitialized()
      as IntegrationTestWidgetsFlutterBinding;
  // IntegrationTestWidgetsFlutterBinding.ensureInitialized();
  FooRobot fooRobot;

  group('Double test flow', () {
    testWidgets("test 1", (WidgetTester tester) async {
      fooRobot = FooRobot(tester);

      await app.main();

      await fooRobot.checkTitle();
      config.reset();
    });
    testWidgets("test 2", (WidgetTester tester) async {
      fooRobot = FooRobot(tester);
      await app.main();

      await fooRobot.checkCounter();
    });
  });
}
